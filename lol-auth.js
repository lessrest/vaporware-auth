var getenv = require('./getenv')
var passport = require('passport')

var PROVIDERS = 'auth0'.split(' ')
var PROVIDER_OPTIONS = { }

passport.serializeUser(function (x, ok) { return ok(null, x) })
passport.deserializeUser(function (x, ok) { return ok(null, x) })

passport.use(new (require('passport-auth0'))({
  domain: getenv('AUTH0_DOMAIN'),
  clientID: getenv('AUTH0_ID'),
  clientSecret: getenv('AUTH0_SECRET'),
  callbackURL: '/auth/auth0/callback'
}, function (accessToken, refreshToken, extraParams, profile, callback) {
  callback(null, profile)
}))

module.exports = function (app) {
  app.use(require('express-session')({
    resave: false, saveUninitialized: false,
    secret: getenv('SESSION_SECRET')
  }))

  app.use(passport.initialize())
  app.use(passport.session())

  PROVIDERS.forEach(function (provider) {
    app.get('/auth/' + provider, passport.authenticate(
      provider, PROVIDER_OPTIONS[provider]
    ))
    app.get('/auth/' + provider + '/callback', passport.authenticate(
      provider, { successRedirect: '/', failureRedirect: '/auth/error' }
    ))
  })

  app.get('/auth/disconnect', function (request, response) {
    request.logout()
    response.redirect('/')
  })

  app.get('/auth/error', function (request, response) {
    // XXX: Handle this more gracefully
    response.status(401).end('Authentication failed')
  })
}
