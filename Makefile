build:; docker build -t vaporware/auth .
run:; docker run -p 80:80 -e PROXY_TARGET=${PROXY_TARGET} -e SESSION_SECRET=${SESSION_SECRET} -e AUTH0_DOMAIN=${AUTH0_DOMAIN} -e AUTH0_ID=${AUTH0_ID} -e AUTH0_SECRET=${AUTH0_SECRET} -it --rm --name vw-auth vaporware/auth
