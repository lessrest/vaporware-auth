var getenv = require('./getenv')
var app = require('express')()
var proxy = require('http-proxy').createProxyServer({})
app.use(require('morgan')('dev'))
require('./lol-auth')(app)
proxy.on('proxyReq', function (proxyReq, req, res, options) {
  proxyReq.setHeader('X-User', JSON.stringify(req.user));
});
proxy.on('error', function (err, req, res) {
  res.writeHead(500, { 'Content-Type': 'text/plain' });
  res.end('Something went wrong.');
});
app.all('*', function (req, res) {
  if (!req.user) res.redirect('/auth/auth0')
  else proxy.web(req, res, { target: getenv('PROXY_TARGET') })
})
app.listen(80, "0.0.0.0")
